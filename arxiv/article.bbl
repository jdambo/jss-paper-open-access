\begin{thebibliography}{50}
\newcommand{\enquote}[1]{``#1''}
\providecommand{\natexlab}[1]{#1}
\providecommand{\url}[1]{\texttt{#1}}
\providecommand{\urlprefix}{URL }
\expandafter\ifx\csname urlstyle\endcsname\relax
  \providecommand{\doi}[1]{doi:\discretionary{}{}{}#1}\else
  \providecommand{\doi}{doi:\discretionary{}{}{}\begingroup
  \urlstyle{rm}\Url}\fi
\providecommand{\eprint}[2][]{\url{#2}}

\bibitem[{Bakar \emph{et~al.}(2016)Bakar, Kokic, and Jin}]{Bakar2016}
Bakar KS, Kokic P, Jin H (2016).
\newblock \enquote{Hierarchical Spatially Varying Coefficient and Temporal
  Dynamic Process Models Using \pkg{spTDyn}.}
\newblock \emph{Journal of Statistical Computation and Simulation},
  \textbf{86}(4), 820--840.
\newblock \doi{10.1080/00949655.2015.1038267}.

\bibitem[{Banerjee \emph{et~al.}(2008)Banerjee, Gelfand, Finley, and
  Sang}]{Banerjee2008}
Banerjee S, Gelfand AE, Finley AO, Sang H (2008).
\newblock \enquote{Gaussian Predictive Process Models for Large Spatial Data
  Sets.}
\newblock \emph{Journal of the Royal Statistical Society: Series B (Statistical
  Methodology)}, \textbf{70}(4), 825--848.
\newblock \doi{10.1111/j.1467-9868.2008.00663.x}.

\bibitem[{Bischl \emph{et~al.}(2016)Bischl, Lang, Kotthoff, Schiffner, Richter,
  Studerus, Casalicchio, and Jones}]{Bischl2016}
Bischl B, Lang M, Kotthoff L, Schiffner J, Richter J, Studerus E, Casalicchio
  G, Jones ZM (2016).
\newblock \enquote{\pkg{mlr}: Machine Learning in \proglang{R}.}
\newblock \emph{Journal of Machine Learning Research}, \textbf{17}(170), 1--5.
\newblock \urlprefix\url{https://jmlr.org/papers/v17/15-066.html}.

\bibitem[{Bischl \emph{et~al.}(2020)Bischl, Lang, Richter, Bossek, Horn, and
  Kerschke}]{R:ParamHelpers}
Bischl B, Lang M, Richter J, Bossek J, Horn D, Kerschke P (2020).
\newblock \emph{\pkg{ParamHelpers}: Helpers for Parameters in Black-Box
  Optimization, Tuning and Machine Learning}.
\newblock \proglang{R} Package Version 1.14,
  \urlprefix\url{https://CRAN.R-project.org/package=ParamHelpers}.

\bibitem[{Bischl \emph{et~al.}(2017)Bischl, Richter, Bossek, Horn, Thomas, and
  Lang}]{R:mlrMBO}
Bischl B, Richter J, Bossek J, Horn D, Thomas J, Lang M (2017).
\newblock \enquote{\pkg{mlrMBO}: A Modular Framework for Model-Based
  Optimization of Expensive Black-Box Functions.}
\newblock \emph{ArXiv Preprint}.
\newblock \urlprefix\url{http://arxiv.org/abs/1703.03373}.

\bibitem[{Bivand \emph{et~al.}(2020)Bivand, Nowosad, and Lovelace}]{R:spData}
Bivand R, Nowosad J, Lovelace R (2020).
\newblock \emph{\pkg{spData}: Datasets for Spatial Analysis}.
\newblock \proglang{R} Package Version 0.3.8,
  \urlprefix\url{https://CRAN.R-project.org/package=spData}.

\bibitem[{Bivand and Yu(2017)}]{R:spgwr}
Bivand R, Yu D (2017).
\newblock \emph{\pkg{spgwr}: Geographically Weighted Regression}.
\newblock \proglang{R} Package Version 0.6-32,
  \urlprefix\url{https://CRAN.R-project.org/package=spgwr}.

\bibitem[{Bondell \emph{et~al.}(2010)Bondell, Krishna, and Ghosh}]{Bondell2010}
Bondell HD, Krishna A, Ghosh SK (2010).
\newblock \enquote{Joint Variable Selection for Fixed and Random Effects in
  Linear Mixed-Effects Models.}
\newblock \emph{Biometrics}, \textbf{66}(4), 1069--1077.
\newblock \doi{10.1111/j.1541-0420.2010.01391.x}.

\bibitem[{Brunsdon \emph{et~al.}(1998)Brunsdon, Fotheringham, and
  Charlton}]{Brundson1998}
Brunsdon C, Fotheringham S, Charlton M (1998).
\newblock \enquote{Geographically Weighted Regression.}
\newblock \emph{Journal of the Royal Statistical Society: Series D (The
  Statistician)}, \textbf{47}(3), 431--443.
\newblock \doi{10.1111/1467-9884.00145}.

\bibitem[{Byrd \emph{et~al.}(1995)Byrd, Lu, Nocedal, and Zhu}]{Byrd1995}
Byrd RH, Lu P, Nocedal J, Zhu C (1995).
\newblock \enquote{A Limited Memory Algorithm for Bound Constrained
  Optimization.}
\newblock \emph{SIAM Journal on Scientific Computing}, \textbf{16}(5),
  1190--1208.
\newblock \doi{10.1137/0916069}.

\bibitem[{Carnell(2020)}]{R:lhs}
Carnell R (2020).
\newblock \emph{\pkg{lhs}: Latin Hypercube Samples}.
\newblock \proglang{R} Package Version 1.1.1,
  \urlprefix\url{https://CRAN.R-project.org/package=lhs}.

\bibitem[{Dambon \emph{et~al.}(2020)Dambon, Fahrl\"ander, Karlen, Lehner,
  Schlesinger, Sigrist, and Zimmermann}]{JAD2020:2}
Dambon JA, Fahrl\"ander SS, Karlen S, Lehner M, Schlesinger J, Sigrist F,
  Zimmermann A (2020).
\newblock \enquote{{Examining the Vintage Effect in Hedonic Pricing using
  Spatially Varying Coefficients Models: A Case Study of Single-Family Houses
  in the Canton of Zurich}.}
\newblock \emph{Preprint}.
\newblock
  \url{https://www.researchgate.net/publication/346392838_Examining_the_Vintage_Effect_in_Hedonic_Pricing_using_Spatially_Varying_Coefficients_Models_A_Case_Study_of_Single-Family_Houses_in_the_Canton_of_Zurich}.

\bibitem[{Dambon \emph{et~al.}(2021{\natexlab{a}})Dambon, Sigrist, and
  Furrer}]{JAD2021}
Dambon JA, Sigrist F, Furrer R (2021{\natexlab{a}}).
\newblock \enquote{{Joint Variable Selection of both Fixed and Random Effects
  for Gaussian Process-based Spatially Varying Coefficient Models}.}
\newblock \emph{ArXiv Preprint}.
\newblock \url{http://arxiv.org/abs/2101.01932}.

\bibitem[{Dambon \emph{et~al.}(2021{\natexlab{b}})Dambon, Sigrist, and
  Furrer}]{JAD2020}
Dambon JA, Sigrist F, Furrer R (2021{\natexlab{b}}).
\newblock \enquote{{Maximum Likelihood Estimation of Spatially Varying
  Coefficient Models for Large Data with an Application to Real Estate Price
  Prediction}.}
\newblock \emph{Spatial Statistics}, \textbf{41}, 100470.
\newblock \doi{10.1016/j.spasta.2020.100470}.

\bibitem[{Datta \emph{et~al.}(2016)Datta, Banerjee, Finley, and
  Gelfand}]{Datta2016}
Datta A, Banerjee S, Finley AO, Gelfand AE (2016).
\newblock \enquote{Hierarchical Nearest-Neighbor Gaussian Process Models for
  Large Geostatistical Datasets.}
\newblock \emph{Journal of the American Statistical Association},
  \textbf{111}(514), 800--812.
\newblock \doi{10.1080/01621459.2015.1044091}.
\newblock PMID: 29720777.

\bibitem[{Finley and Banerjee(2020)}]{Finley2020}
Finley AO, Banerjee S (2020).
\newblock \enquote{Bayesian Spatially Varying Coefficient Models in the
  \pkg{spBayes} \proglang{R} Rackage.}
\newblock \emph{Environmental Modelling \& Software}, \textbf{125}, 104608.
\newblock ISSN 1364-8152.
\newblock \doi{10.1016/j.envsoft.2019.104608}.

\bibitem[{Finley \emph{et~al.}(2015)Finley, Banerjee, and Gelfand}]{Finley2015}
Finley AO, Banerjee S, Gelfand AE (2015).
\newblock \enquote{\pkg{spBayes} for Large Univariate and Multivariate
  Point-Referenced Spatio-Temporal Data Models.}
\newblock \emph{Journal of Statistical Software}, \textbf{63}(13), 1--28.
\newblock ISSN 1548-7660.
\newblock \doi{10.18637/jss.v063.i13}.

\bibitem[{Friedman \emph{et~al.}(2010)Friedman, Hastie, and
  Tibshirani}]{Friedman2010}
Friedman J, Hastie T, Tibshirani R (2010).
\newblock \enquote{Regularization Paths for Generalized Linear Models via
  Coordinate Descent.}
\newblock \emph{Journal of Statistical Software}, \textbf{33}(1), 1--22.
\newblock ISSN 1548-7660.
\newblock \doi{10.18637/jss.v033.i01}.

\bibitem[{Furrer \emph{et~al.}(2006)Furrer, Genton, and Nychka}]{Furrer2006}
Furrer R, Genton MG, Nychka DW (2006).
\newblock \enquote{{Covariance Tapering for Interpolation of Large Spatial
  Datasets}.}
\newblock \emph{Journal of Computational and Graphical Statistics},
  \textbf{15}(3), 502--523.
\newblock \doi{10.1198/106186006X132178}.

\bibitem[{Furrer and Sain(2010)}]{R:spam}
Furrer R, Sain SR (2010).
\newblock \enquote{\pkg{spam}: A Sparse Matrix {\proglang{R}} Package with
  Emphasis on {MCMC} Methods for {G}aussian {M}arkov Random Fields.}
\newblock \emph{Journal of Statistical Software}, \textbf{36}(10), 1--25.
\newblock ISSN 1548-7660.
\newblock \doi{10.18637/jss.v036.i10}.

\bibitem[{Gelfand \emph{et~al.}(2003)Gelfand, Kim, Sirmans, and
  Banerjee}]{Gelfand2003}
Gelfand AE, Kim HJ, Sirmans CF, Banerjee S (2003).
\newblock \enquote{Spatial Modeling with Spatially Varying Coefficient
  Processes.}
\newblock \emph{Journal of the American Statistical Association},
  \textbf{98}(462), 387--396.
\newblock ISSN 0162-1459.
\newblock \doi{10.1198/016214503000170}.

\bibitem[{Gelfand and Schliep(2016)}]{Gelfand2016}
Gelfand AE, Schliep EM (2016).
\newblock \enquote{Spatial Statistics and Gaussian Processes: A Beautiful
  Marriage.}
\newblock \emph{Spatial Statistics}, \textbf{18}, 86 -- 104.
\newblock ISSN 2211-6753.
\newblock \doi{10.1016/j.spasta.2016.03.006}.
\newblock Spatial Statistics Avignon: Emerging Patterns.

\bibitem[{Gerber and Furrer(2019)}]{R:optimParallel}
Gerber F, Furrer R (2019).
\newblock \enquote{{\pkg{optimParallel}: An \proglang{R} Package Providing a
  Parallel Version of the \code{L-BFGS-B} Optimization Method}.}
\newblock \emph{The \proglang{R} Journal}, \textbf{11}(1), 352--358.
\newblock \doi{10.32614/RJ-2019-030}.

\bibitem[{Gollini \emph{et~al.}(2015)Gollini, Lu, Charlton, Brunsdon, and
  Harris}]{R:GWmodel}
Gollini I, Lu B, Charlton M, Brunsdon C, Harris P (2015).
\newblock \enquote{\pkg{GWmodel}: An {\proglang{R}} Package for Exploring
  Spatial Heterogeneity Using Geographically Weighted Models.}
\newblock \emph{Journal of Statistical Software}, \textbf{63}(17), 1--50.
\newblock ISSN 1548-7660.
\newblock \doi{10.18637/jss.v063.i17}.

\bibitem[{Hastie and Tibshirani(1993)}]{Hastie1993}
Hastie T, Tibshirani R (1993).
\newblock \enquote{Varying-Coefficient Models.}
\newblock \emph{Journal of the Royal Statistical Society. Series B
  (Methodological)}, \textbf{55}(4), 757--796.
\newblock ISSN 0035-9246.
\newblock \doi{10.1111/j.2517-6161.1993.tb01939.x}.

\bibitem[{Heaton \emph{et~al.}(2019)Heaton, Datta, Finley, Furrer, Guinness,
  Guhaniyogi, Gerber, Gramacy, Hammerling, Katzfuss, Lindgren, Nychka, Sun, and
  Zammit-Mangion}]{Heaton2019}
Heaton MJ, Datta A, Finley AO, Furrer R, Guinness J, Guhaniyogi R, Gerber F,
  Gramacy RB, Hammerling D, Katzfuss M, Lindgren FK, Nychka DW, Sun F,
  Zammit-Mangion A (2019).
\newblock \enquote{A Case Study Competition Among Methods for Analyzing Large
  Spatial Data.}
\newblock \emph{Journal of Agricultural, Biological and Environmental
  Statistics}, \textbf{24}(3), 398--425.
\newblock ISSN 1537-2693.
\newblock \doi{10.1007/s13253-018-00348-w}.

\bibitem[{{Horn} and {Bischl}(2016)}]{Horn2016}
{Horn} D, {Bischl} B (2016).
\newblock \enquote{Multi-Objective Parameter Configuration of Machine Learning
  Algorithms using Model-Based Optimization.}
\newblock In \emph{2016 IEEE Symposium Series on Computational Intelligence
  (SSCI)}, pp. 1--8.
\newblock \doi{10.1109/SSCI.2016.7850221}.

\bibitem[{Hothorn \emph{et~al.}(2021)Hothorn, B\"uhlmann, Kneib, Schmid, and
  Hofner}]{R:mboost}
Hothorn T, B\"uhlmann P, Kneib T, Schmid M, Hofner B (2021).
\newblock \emph{\pkg{mboost}: Model-Based Boosting}.
\newblock \proglang{R} Package Version 2.9-5,
  \urlprefix\url{https://CRAN.R-project.org/package=mboost}.

\bibitem[{Hyndman and Athanasopoulos(2018)}]{Hyndman2018}
Hyndman RJ, Athanasopoulos G (2018).
\newblock \emph{Forecasting: Principles and Practice}.
\newblock 2 edition. OTexts.
\newblock \urlprefix\url{https://otexts.com/fpp2/}.

\bibitem[{Ibrahim \emph{et~al.}(2011)Ibrahim, Zhu, Garcia, and
  Guo}]{Ibrahim2011}
Ibrahim JG, Zhu H, Garcia RI, Guo R (2011).
\newblock \enquote{Fixed and Random Effects Selection in Mixed Effects Models.}
\newblock \emph{Biometrics}, \textbf{67}(2), 495--503.
\newblock \doi{10.1111/j.1541-0420.2010.01463.x}.

\bibitem[{Jones(2001)}]{Jones2001}
Jones DR (2001).
\newblock \enquote{A Taxonomy of Global Optimization Methods Based on Response
  Surfaces.}
\newblock \emph{Journal of Global Optimization}, \textbf{21}, 345–383.
\newblock \doi{10.1023/A:1012771025575}.

\bibitem[{Koch \emph{et~al.}(2012)Koch, Bischl, Flasch, Bartz-Beielstein,
  Weihs, and Konen}]{Koch2012}
Koch P, Bischl B, Flasch O, Bartz-Beielstein T, Weihs C, Konen W (2012).
\newblock \enquote{Tuning and Evolution of Support Vector Kernels.}
\newblock \emph{Evolutionary Intelligence}, \textbf{5}(3), 153--170.
\newblock \doi{10.1007/s12065-012-0073-8}.

\bibitem[{Lasinio \emph{et~al.}(2013)Lasinio, Mastrantonio, and
  Pollice}]{Lasinio2013}
Lasinio GJ, Mastrantonio G, Pollice A (2013).
\newblock \enquote{Discussing the ``big $n$ problem''.}
\newblock \emph{Statistical Methods \& Applications}, \textbf{22}(1), 97--112.
\newblock \doi{10.1007/s10260-012-0207-2}.

\bibitem[{Lindgren and Rue(2015)}]{R:INLA}
Lindgren FK, Rue H (2015).
\newblock \enquote{Bayesian Spatial Modelling with \proglang{R}-\pkg{INLA}.}
\newblock \emph{Journal of Statistical Software}, \textbf{63}(19), 1--25.
\newblock ISSN 1548-7660.
\newblock \doi{10.18637/jss.v063.i19}.

\bibitem[{Lindgren \emph{et~al.}(2011)Lindgren, Rue, and
  Lindstr{\"o}m}]{Lindgren2011}
Lindgren FK, Rue H, Lindstr{\"o}m J (2011).
\newblock \enquote{{An Explicit Link between {G}aussian Fields and {G}aussian
  {M}arkov Random Fields: The Stochastic Partial Differential Equation
  Approach}.}
\newblock \emph{Journal of the Royal Statistical Society: Series B (Statistical
  Methodology)}, \textbf{73}(4), 423--498.
\newblock \doi{10.1111/j.1467-9868.2011.00777.x}.

\bibitem[{Müller \emph{et~al.}(2013)Müller, Scealy, and Welsh}]{Mueller2013}
Müller S, Scealy JL, Welsh AH (2013).
\newblock \enquote{Model Selection in Linear Mixed Models.}
\newblock \emph{Statist. Sci.}, \textbf{28}(2), 135--167.
\newblock \doi{10.1214/12-STS410}.

\bibitem[{Ng and Peyton(1993)}]{NG1993}
Ng EG, Peyton BW (1993).
\newblock \enquote{Block Pparse Cholesky Algorithms on Advanced Uniprocessor
  Computers.}
\newblock \emph{SIAM Journal on Scientific Computing}, \textbf{14}(5),
  1034--1056.
\newblock \doi{10.1137/0914063}.

\bibitem[{Rasmussen and Williams(2005)}]{Rasmussen2005}
Rasmussen CE, Williams CKI (2005).
\newblock \emph{Gaussian Processes for Machine Learning (Adaptive Computation
  and Machine Learning)}.
\newblock The MIT Press.
\newblock ISBN 026218253X.

\bibitem[{{\proglang{R} Core Team}(2021)}]{R}
{\proglang{R} Core Team} (2021).
\newblock \emph{\proglang{R}: {A} Language and Environment for Statistical
  Computing}.
\newblock \proglang{R} Foundation for Statistical Computing, Vienna, Austria.
\newblock \urlprefix\url{https://www.R-project.org/}.

\bibitem[{Rey and Anselin(2010)}]{Rey2010}
Rey SJ, Anselin L (2010).
\newblock \emph{\pkg{PySAL}: A \proglang{Python} Library of Spatial Analytical
  Methods}, pp. 175--193.
\newblock Springer Berlin Heidelberg, Berlin, Heidelberg.
\newblock ISBN 978-3-642-03647-7.
\newblock \doi{10.1007/978-3-642-03647-7_11}.

\bibitem[{Roberts \emph{et~al.}(2013)Roberts, Osborne, Ebden, Reece, Gibson,
  and Aigrain}]{Roberts2013}
Roberts S, Osborne M, Ebden M, Reece S, Gibson N, Aigrain S (2013).
\newblock \enquote{Gaussian Processes for Time-Series Modelling.}
\newblock \emph{Philosophical Transactions of the Royal Society A:
  Mathematical, Physical and Engineering Sciences}, \textbf{371}(1984),
  20110550.
\newblock \doi{10.1098/rsta.2011.0550}.

\bibitem[{Rue and Held(2005)}]{Rue2005}
Rue H, Held L (2005).
\newblock \emph{Gaussian Markov Random Fields: Theory and Applications}.
\newblock Chapman \& Hall/CRC Monographs on Statistics \& Applied Probability.
  CRC Press.
\newblock ISBN 9780203492024.

\bibitem[{Rue \emph{et~al.}(2017)Rue, Riebler, S{\o}rbye, Illian, Simpson, and
  Lindgren}]{INLA:review2016}
Rue H, Riebler A, S{\o}rbye SH, Illian JB, Simpson DP, Lindgren FK (2017).
\newblock \enquote{{B}ayesian Computing with \pkg{INLA}: A Review.}
\newblock \emph{Annual Review of Statistics and Its Application},
  \textbf{4}(1), 395--421.
\newblock \doi{10.1146/annurev-statistics-060116-054045}.

\bibitem[{Schlather \emph{et~al.}(2015)Schlather, Malinowski, Menck, Oesting,
  and Strokorb}]{R:RandomFields}
Schlather M, Malinowski A, Menck PJ, Oesting M, Strokorb K (2015).
\newblock \enquote{Analysis, Simulation and Prediction of Multivariate Random
  Fields with Package \pkg{RandomFields}.}
\newblock \emph{Journal of Statistical Software}, \textbf{63}(8), 1--25.
\newblock ISSN 1548-7660.
\newblock \doi{10.18637/jss.v063.i08}.

\bibitem[{Tibshirani(1996)}]{Tibshirani1996}
Tibshirani R (1996).
\newblock \enquote{Regression Shrinkage and Selection via the Lasso.}
\newblock \emph{Journal of the Royal Statistical Society. Series B
  (Methodological)}, \textbf{58}(1), 267--288.
\newblock ISSN 0035-9246.
\newblock \doi{10.1111/j.2517-6161.1996.tb02080.x}.

\bibitem[{Vaida and Blanchard(2005)}]{Vaida2005}
Vaida F, Blanchard S (2005).
\newblock \enquote{Conditional Akaike Information for Mixed-Effects Models.}
\newblock \emph{Biometrika}, \textbf{92}(2), 351--370.
\newblock \doi{10.1093/biomet/92.2.351}.

\bibitem[{Wheeler(2013)}]{R:gwrr}
Wheeler DC (2013).
\newblock \emph{\pkg{gwrr}: Fits Geographically Weighted Regression Models with
  Diagnostic Tools}.
\newblock \proglang{R} Package Version 0.2-1,
  \urlprefix\url{https://CRAN.R-project.org/package=gwrr}.

\bibitem[{Wood(2017)}]{Wood2017}
Wood SN (2017).
\newblock \emph{Generalized Additive Models: An Introduction with
  \proglang{R}}.
\newblock 2 edition. Chapman and Hall/CRC.

\bibitem[{Wu \emph{et~al.}(2014)Wu, Lobato, and Ghahramani}]{Wu2014}
Wu Y, Lobato JMH, Ghahramani Z (2014).
\newblock \enquote{Gaussian Process Volatility Model.}
\newblock In \emph{Proceedings of the 27th International Conference on Neural
  Information Processing Systems - Volume 1}, NIPS'14, p. 1044–1052. MIT
  Press, Cambridge, MA, USA.
\newblock
  \urlprefix\url{https://proceedings.neurips.cc/paper/2014/file/a733fa9b25f33689e2adbe72199f0e62-Paper.pdf}.

\bibitem[{Zou(2006)}]{Zou2006}
Zou H (2006).
\newblock \enquote{The Adaptive Lasso and Its Oracle Properties.}
\newblock \emph{Journal of the American Statistical Association},
  \textbf{101}(476), 1418--1429.
\newblock \doi{10.1198/016214506000000735}.

\end{thebibliography}
