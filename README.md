# JSS Paper Open Access

To compute the examples of the Lucas County and US Change data, please:

- run the following scripts in folder data-analysis/lucas-county in given order:
    - SVC_model.R
    - SVC_analyze.R
    - SVC_plot.R
- run data-analysis/uschange/var-sel-uschange.R